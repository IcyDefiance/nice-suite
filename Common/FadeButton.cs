﻿using System;
using System.Collections.Generic;
using OpenTK;
using OpenTK.Graphics;
using nIceGame;
using Common;
using Common.Scripts;

namespace nIceSuite
{
    public class FadeButton : Button
    {
        private Entity text, background, highlight;
        bool disposed = false;

        public FadeButton(Rectangle area, string text)
            : base(area)
        {
            this.text = new Entity();
            this.text.Set(new Text(text));
            this.text.Set(new Transform(new Vector3(area.X + 10, area.Y + 10, 10), Quaternion.Identity, Vector3.One));
            this.text.Set(new Tint(1, 1, 1, 0));
            List<Script> scripts = new List<Script>();
            scripts.Add(new FadeTo(this.text, 1, 2.4f));
            this.text.Set(scripts);
            Engine.Entities.Add(this.text);
            
            background = new Entity();
            background.Set(Texture2D.WhitePixel);
            background.Set(new Transform(new Vector3(area.X, area.Y, 0), Quaternion.Identity, new Vector3(area.Width, area.Height, 1)));
            background.Set(new Tint(0, 0, 0, 0));
            List<Script> bgScripts = new List<Script>();
            bgScripts.Add(new FadeTo(background, 0.5f, 2.4f));
            background.Set(bgScripts);
            Engine.Entities.Add(background);

            highlight = new Entity();
            highlight.Set(Texture2D.WhitePixel);
            highlight.Set(new Transform(new Vector3(area.X, area.Y, 0), Quaternion.Identity, new Vector3(area.Width, area.Height, 1)));
            highlight.Set(new Tint(0, 0.75f, 1, 0));
            highlight.Set(new List<Script>());
            Engine.Entities.Add(highlight);

            OnMouseOver += SolitaireButton_OnMouseOver;
            OnMouseOut += SolitaireButton_OnMouseOut;
        }

        void SolitaireButton_OnMouseOver(object sender, EventArgs e)
        {
            ScriptList.ReplaceSameType(highlight.Get<List<Script>>(), new FadeTo(highlight, 0.25f, 4.8f));
        }

        void SolitaireButton_OnMouseOut(object sender, EventArgs e)
        {
            ScriptList.ReplaceSameType(highlight.Get<List<Script>>(), new FadeTo(highlight, 0, 4.8f));
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            if (disposed)
                return;

            if (disposing)
            {
                Engine.Entities.Remove(text);
                Engine.Entities.Remove(background);
                Engine.Entities.Remove(highlight);
                OnMouseOver -= SolitaireButton_OnMouseOver;
                OnMouseOut -= SolitaireButton_OnMouseOut;
            }

            disposed = true;
        }
    }
}

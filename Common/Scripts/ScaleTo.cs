﻿using nIceGame;
using System;
using System.Collections.Generic;
using OpenTK;

namespace Common.Scripts
{
    public class ScaleTo : Script
    {
        Vector3 target, speed;
        bool xHit, yHit, zHit;

        public ScaleTo(Entity parent, Vector3 target, Vector3 speed) : base(parent)
        {
            this.target = target;
            this.speed = speed;
        }

        public override void Draw(double delta)
        {
            Transform transform = Parent.Get<Transform>();
            transform.Scale = Vector3.Multiply(transform.Scale, Vector3.One - speed);
            if (transform.Scale.X <= target.X)
            {
                transform.Scale = new Vector3(target.X, transform.Scale.Y, transform.Scale.Z);
                xHit = true;
            }
            if (transform.Scale.Y <= target.Y)
            {
                transform.Scale = new Vector3(transform.Scale.X, target.Y, transform.Scale.Z);
                yHit = true;
            }
            if (transform.Scale.Z <= target.Z)
            {
                transform.Scale = new Vector3(transform.Scale.X, transform.Scale.Y, target.Z);
                zHit = true;
            }

            if (xHit && yHit && zHit)
                Die();
        }

        public override void Update(double delta)
        {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using nIceGame;

namespace Common.Scripts
{
    public static class ScriptList
    {
        public static bool HasScriptType(List<Script> scripts, Type type)
        {
            return scripts.FindIndex(x => x.GetType() == type) != -1;
        }

        public static void RemoveScriptType(List<Script> scripts, Type type)
        {
            scripts.RemoveAll(x => x.GetType() == type);
        }

        public static void ReplaceSameType(List<Script> scripts, Script toAdd)
        {
            int replaceIndex = scripts.FindIndex(x => x.GetType() == toAdd.GetType());
            if (replaceIndex == -1)
                scripts.Add(toAdd);
            else
                scripts[replaceIndex] = toAdd;
        }
    }
}

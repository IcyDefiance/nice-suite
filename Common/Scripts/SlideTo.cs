﻿using nIceGame;
using OpenTK;

namespace Common.Scripts
{
    public class SlideTo : Script
    {
        private Vector3 target;
        private float speed;

        public SlideTo(Entity parent, Vector3 target, float speed) : base(parent)
        {
            this.target = target;
            this.speed = speed;
        }

        public override void Update(double delta)
        {
            Transform transform = Parent.Get<Transform>();
            if ((target - transform.Position).LengthFast >= 1f)
                transform.Position += Vector3.Multiply(target - transform.Position, speed);
            else
                Die();
        }

        public override void Draw(double delta)
        {
        }
    }
}

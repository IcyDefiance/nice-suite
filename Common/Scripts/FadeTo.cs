﻿using System;
using nIceGame;
using OpenTK;
using OpenTK.Graphics;

namespace Common.Scripts
{
    public class FadeTo : Script
    {
        Tint target;
        float speed;

        public FadeTo(Entity parent, Tint target, float speed) : base(parent)
        {
            this.target = target;
            this.speed = speed;
        }

        public FadeTo(Entity parent, float alphaTarget, float speed)
            : base(parent)
        {
            Tint current = parent.Get<Tint>();
            this.target = new Tint(current.R, current.G, current.B, alphaTarget);
            this.speed = speed;
        }

        public override void Update(double delta)
        {
            Tint current = Parent.Get<Tint>();
            if (current == null)
                current = Tint.White;

            current.R += (target.R - current.R) * speed / 60;
            current.G += (target.G - current.G) * speed / 60;
            current.B += (target.B - current.B) * speed / 60;
            current.W += (target.W - current.W) * speed / 60;

            if (Math.Abs(current.R - target.R) < 0.01f && Math.Abs(current.G - target.G) < 0.01f && Math.Abs(current.B - target.B) < 0.01f && Math.Abs(current.W - target.W) < 0.01f)
                Die();
            
            Parent.Set(current);
        }

        public override void Draw(double delta)
        {
        }
    }
}

﻿using System;
using System.Collections.Generic;
using nIceGame;
using OpenTK;
using OpenTK.Input;

namespace Common
{
    public class Button : IDisposable
    {
        public event EventHandler OnMouseOver, OnMouseOut, OnLeftClick, OnRightClick;
        private Rectangle buttonArea;
        private bool mouseOver;
        private bool disposed = false;

        public Button(Rectangle buttonArea)
        {
            mouseOver = false;
            this.buttonArea = buttonArea;

            Engine.GameWindow.Mouse.ButtonDown += Mouse_ButtonDown;
            Engine.GameWindow.Mouse.Move += Mouse_Move;
        }

        private void Mouse_ButtonDown(object sender, OpenTK.Input.MouseButtonEventArgs e)
        {
            if (mouseOver && e.Button == MouseButton.Left && OnLeftClick != null)
            {
                OnLeftClick(this, new EventArgs());
            }
            else if (mouseOver && e.Button == MouseButton.Right && OnRightClick != null)
            {
                OnRightClick(this, new EventArgs());
            }
        }

        private void Mouse_Move(object sender, OpenTK.Input.MouseMoveEventArgs e)
        {
            if (!mouseOver && buttonArea.Contains(e.X, e.Y))
            {
                mouseOver = true;
                if (OnMouseOver != null)
                    OnMouseOver(this, new EventArgs());
            }
            else if (mouseOver && !buttonArea.Contains(e.X, e.Y))
            {
                mouseOver = false;
                if (OnMouseOut != null)
                    OnMouseOut(this, new EventArgs());
            }
        }

        public Rectangle ButtonArea
        {
            get { return buttonArea; }
            set { buttonArea = value; }
        }

        public bool MouseOver
        {
            get { return mouseOver; }
            private set { mouseOver = value; }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                Engine.GameWindow.Mouse.ButtonDown -= Mouse_ButtonDown;
                Engine.GameWindow.Mouse.Move -= Mouse_Move;
            }

            disposed = true;
        }
    }
}

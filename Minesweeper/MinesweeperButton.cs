﻿using System;
using System.Collections.Generic;
using OpenTK;
using OpenTK.Graphics;
using nIceGame;
using Common;
using Common.Scripts;

namespace nIceSuite
{
    public class MinesweeperButton : Button
    {
        public int PosX { get; private set; }
        public int PosY { get; private set; }
        public bool Clicked { get; private set; }
        public bool Frozen { get; set; }

        private int number;
        private bool flagged;

        Texture2D mineTex, flagTex;
        Entity symbol, background, flag;
        bool disposed;
        Rectangle area;

        public MinesweeperButton(Rectangle area, Texture2D mineTex, Texture2D flagTex, int x, int y)
            : base(area)
        {
            this.area = area;
            this.mineTex = mineTex;
            this.flagTex = flagTex;
            this.PosX = x;
            this.PosY = y;
            this.Clicked = false;

            background = new Entity();
            background.Set(Texture2D.WhitePixel);
            background.Set(new Transform(new Vector3(area.X, area.Y, 18), Quaternion.Identity, new Vector3(area.Width, area.Height, 1)));
            background.Set(new Tint(0, 0.5f, 1, 0.5f));
            background.Set(new List<Script>());
            Engine.Entities.Add(background);

            flag = new Entity();
            flag.Set(flagTex);
            flag.Set(new Transform(new Vector3(area.X, area.Y, 21), Quaternion.Identity, Vector3.One));

            OnMouseOver += NumberButton_OnMouseOver;
            OnMouseOut += NumberButton_OnMouseOut;
        }

        public void Trigger()
        {
            if (Clicked)
                return;
            Clicked = true;

            ScriptList.RemoveScriptType(background.Get<List<Script>>(), typeof(FadeTo));
            if (number >= 0)
                background.Set(new Tint(1, 1, 1, 0.7f));
            else
                background.Set(new Tint(1, 0, 0, 0.7f));

            Frozen = true;

            if (Number != 0)
                Engine.Entities.Add(symbol);
        }

        public void SetGreen()
        {
            background.Set(new Tint(0, 0.6f, 0, 0.7f));
        }

        void NumberButton_OnMouseOver(object sender, EventArgs e)
        {
            if (!Frozen)
                ScriptList.ReplaceSameType(background.Get<List<Script>>(), new FadeTo(background, new Tint(0.5f, 0.75f, 1, 0.75f), 4.8f));
        }

        void NumberButton_OnMouseOut(object sender, EventArgs e)
        {
            if (!Frozen)
                ScriptList.ReplaceSameType(background.Get<List<Script>>(), new FadeTo(background, new Tint(0, 0.5f, 1, 0.5f), 4.8f));
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            if (disposed)
                return;

            if (disposing)
            {
                if (symbol != null)
                    Engine.Entities.Remove(symbol);
                Engine.Entities.Remove(background);
                Engine.Entities.Remove(flag);
                OnMouseOver -= NumberButton_OnMouseOver;
                OnMouseOut -= NumberButton_OnMouseOut;
            }

            disposed = true;
        }

        public int Number
        {
            get { return number; }
            set
            {
                if (value > 0)
                {
                    symbol = new Entity();
                    symbol.Set(new Text(value.ToString(), Vector4.One));
                    symbol.Set(new Transform(new Vector3(area.X + 5, area.Y + 3, 20), Quaternion.Identity, Vector3.One));
                    symbol.Set(new Tint(Color4.Black));
                }
                else if (value < 0)
                {
                    symbol = new Entity();
                    symbol.Set(mineTex);
                    symbol.Set(new Transform(new Vector3(area.X, area.Y, 20), Quaternion.Identity, Vector3.One));
                }
                else
                {
                    symbol = null;
                }
                number = value;
            }
        }

        public bool Flagged
        {
            get { return flagged; }
            set
            {
                if (value && !flagged)
                {
                    Engine.Entities.Add(flag);
                }
                else if (!value && flagged)
                {
                    Engine.Entities.Remove(flag);
                }

                flagged = value;
            }
        }
    }
}

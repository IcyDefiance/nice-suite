﻿using System;
using System.Collections.Generic;
using nIceGame;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using Common.Scripts;

namespace nIceSuite
{
    public class MinesweeperScene : Scene
    {
        Entity background, logo;
        FadeButton newGameButton, mainMenuButton;
        Systems.BoardManager boardManager;
        bool disposed = false;

        public MinesweeperScene(Entity background, Entity logo)
        {
            this.background = background;
            this.logo = logo;
        }

        public override void OnLoad()
        {
            base.OnLoad();

            Engine.GameWindow.ClientSize = new System.Drawing.Size(1090, 680);

            Engine.Entities.Add(background);
            Engine.Entities.Add(logo);
            float scale = Math.Max((float)Engine.GameWindow.Height / background.Get<Texture2D>().Height, (float)Engine.GameWindow.Width / background.Get<Texture2D>().Width);
            background.Set(new Transform(Vector3.Zero, Quaternion.Identity, new Vector3(scale, scale, 1)));

            //board back
            Entity board = new Entity();
            board.Set(Texture2D.WhitePixel);
            board.Set(new Transform(new Vector3(25, 70, 0), Quaternion.Identity, new Vector3(Engine.GameWindow.Width - 50, Engine.GameWindow.Height - 95, 1)));
            board.Set(new Tint(0, 0, 0, 0));
            List<Script> boardScripts = new List<Script>();
            boardScripts.Add(new FadeTo(board, 0.5f, 2.4f));
            board.Set(boardScripts);
            Engine.Entities.Add(board);

            //new game button
            newGameButton = new FadeButton(new Rectangle(770, 25, 150, 45), "New Game");
            newGameButton.OnLeftClick += NewGameButton_OnClick;

            //main menu button
            mainMenuButton = new FadeButton(new Rectangle(610, 25, 150, 45), "Main Menu");
            mainMenuButton.OnLeftClick += mainMenuButton_OnClick;

            //board manager
            Engine.Systems.Add(new MeshRenderer());
            Engine.Systems.Add(new SpriteRenderer());
            Engine.Systems.Add(new ScriptRunner());
            boardManager = new Systems.BoardManager();
            Engine.Systems.Add(boardManager);
        }

        public override void OnResize(object sender, EventArgs e)
        {
            base.OnResize(sender, e);
        }

        public override void UpdateFrame(object sender, OpenTK.FrameEventArgs e)
        {
            base.UpdateFrame(sender, e);
        }

        public override void RenderFrame(object sender, OpenTK.FrameEventArgs e)
        {
            base.RenderFrame(sender, e);

            Engine.GameWindow.SwapBuffers();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            if (disposed)
                return;

            if (disposing)
            {
                newGameButton.OnLeftClick -= NewGameButton_OnClick;
                mainMenuButton.OnLeftClick -= mainMenuButton_OnClick;
                newGameButton.Dispose();
                mainMenuButton.Dispose();
            }

            disposed = true;
        }

        private void NewGameButton_OnClick(object sender, EventArgs e)
        {
            boardManager.NewGame();
        }

        private void mainMenuButton_OnClick(object sender, EventArgs e)
        {
            Engine.PopScene();
        }
    }
}

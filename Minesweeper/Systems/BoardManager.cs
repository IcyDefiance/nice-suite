﻿using System;
using System.Timers;
using System.Collections.Generic;
using nIceGame;
using OpenTK;
using OpenTK.Input;
using Common;
using Common.Scripts;

namespace nIceSuite.Systems
{
    class BoardManager : ISystem
    {
        bool disposed;
        const int boardWidth = 30, boardHeight = 16, boardX = 50, boardY = 100, buttonWidth = 32, buttonHeight = 32, buttonPadding = 1, mineCount = 99;
        bool firstClickDone;
        MinesweeperButton[,] board;
        Texture2D mineTex, flagTex;

        public BoardManager()
        {
            Initialize();
        }

        public void OnResize()
        {

        }

        public void OnUpdate(double delta)
        {
        }

        public void OnRender(double delta)
        {

        }

        public void NewGame()
        {
            ResetBoard();
        }

        private void Initialize()
        {
            mineTex = Content.LoadTexture2D("minesweeper\\mine", true);
            flagTex = Content.LoadTexture2D("minesweeper\\flag", true);

            ResetBoard();
        }

        void ResetBoard()
        {
            if (board != null)
                for (int x = 0; x < boardWidth; x++)
                    for (int y = 0; y < boardHeight; y++)
                        board[x, y].Dispose();

            board = new MinesweeperButton[boardWidth, boardHeight];
            for (int x = 0; x < boardWidth; x++)
            {
                for (int y = 0; y < boardHeight; y++)
                {
                    Rectangle area = new Rectangle((buttonWidth + buttonPadding) * x + boardX, (buttonHeight + buttonPadding) * y + boardY, buttonWidth, buttonHeight);
                    board[x, y] = new MinesweeperButton(area, mineTex, flagTex, x, y);
                    board[x, y].OnLeftClick += numberButton_OnLeftClick;
                    board[x, y].OnRightClick += BoardManager_OnRightClick;
                }
            }

            firstClickDone = false;
        }

        void GenerateMines()
        {
            //clear
            for (int x = 0; x < boardWidth; x++)
            {
                for (int y = 0; y < boardHeight; y++)
                {
                    board[x, y].Number = 0;
                }
            }

            //place mines
            Random r = new Random();
            for (int i = 0; i < mineCount; i++)
            {
                int x, y;
                do
                {
                    x = r.Next(boardWidth);
                    y = r.Next(boardHeight);
                } while (board[x, y].Number == -1);

                board[x, y].Number = -1; ;
            }

            //place numbers
            for (int x = 0; x < boardWidth; x++)
            {
                for (int y = 0; y < boardHeight; y++)
                {
                    if (board[x, y].Number > -1)
                    {
                        int number = 0;
                        if (x > 0 && y > 0 && board[x - 1, y - 1].Number < 0) number++;
                        if (y > 0 && board[x, y - 1].Number < 0) number++;
                        if (x < boardWidth - 1 && y > 0 && board[x + 1, y - 1].Number < 0) number++;
                        if (x < boardWidth - 1 && board[x + 1, y].Number < 0) number++;
                        if (x < boardWidth - 1 && y < boardHeight - 1 && board[x + 1, y + 1].Number < 0) number++;
                        if (y < boardHeight - 1 && board[x, y + 1].Number < 0) number++;
                        if (x > 0 && y < boardHeight - 1 && board[x - 1, y + 1].Number < 0) number++;
                        if (x > 0 && board[x - 1, y].Number < 0) number++;

                        board[x, y].Number = number;
                    }
                }
            }
        }

        void numberButton_OnLeftClick(object sender, EventArgs e)
        {
            MinesweeperButton button = sender as MinesweeperButton;
            if (button.Frozen || button.Flagged)
                return;
            else if (!firstClickDone)
                HandleFirstClick(button.PosX, button.PosY);

            if (button.Number == 0)
                HandleClick0(button.PosX, button.PosY);
            else if (button.Number > 0)
                button.Trigger();
            else
            {
                for (int x = 0; x < boardWidth; x++)
                {
                    for (int y = 0; y < boardHeight; y++)
                    {
                        board[x, y].Frozen = true;
                        if (board[x, y].Number < 0)
                            board[x, y].Trigger();
                    }
                }
            }

            if (CheckForWin())
            {
                for (int x = 0; x < boardWidth; x++)
                {
                    for (int y = 0; y < boardHeight; y++)
                    {
                        board[x, y].Frozen = true;
                        if (board[x, y].Number >= 0)
                            board[x, y].SetGreen();
                    }
                }
            }
        }

        void BoardManager_OnRightClick(object sender, EventArgs e)
        {
            MinesweeperButton button = sender as MinesweeperButton;
            if (!button.Clicked)
                button.Flagged = !button.Flagged;
        }

        void HandleClick0(int x, int y)
        {
            if (!board[x, y].Clicked)
            {
                board[x, y].Trigger();

                if (board[x, y].Number == 0)
                {
                    if (x > 0 && y > 0 && board[x - 1, y - 1].Number >= 0) HandleClick0(x - 1, y - 1);
                    if (y > 0 && board[x, y - 1].Number >= 0) HandleClick0(x, y - 1);
                    if (x < boardWidth - 1 && y > 0 && board[x + 1, y - 1].Number >= 0) HandleClick0(x + 1, y - 1);
                    if (x < boardWidth - 1 && board[x + 1, y].Number >= 0) HandleClick0(x + 1, y);
                    if (x < boardWidth - 1 && y < boardHeight - 1 && board[x + 1, y + 1].Number >= 0) HandleClick0(x + 1, y + 1);
                    if (y < boardHeight - 1 && board[x, y + 1].Number >= 0) HandleClick0(x, y + 1);
                    if (x > 0 && y < boardHeight - 1 && board[x - 1, y + 1].Number >= 0) HandleClick0(x - 1, y + 1);
                    if (x > 0 && board[x - 1, y].Number >= 0) HandleClick0(x - 1, y);
                }
            }
        }

        void HandleFirstClick(int x, int y)
        {
            do
            {
                GenerateMines();
            } while (board[x, y].Number != 0);
            firstClickDone = true;
        }

        bool CheckForWin()
        {
            bool win = true;

            for (int x = 0; x < boardWidth && win; x++)
                for (int y = 0; y < boardHeight && win; y++)
                    if (board[x, y].Number >= 0 && !board[x, y].Clicked)
                        win = false;

            return win;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                for (int x = 0; x < boardWidth; x++)
                {
                    for (int y = 0; y < boardHeight; y++)
                    {
                        board[x, y].Dispose();
                    }
                }
            }

            disposed = true;
        }
    }
}

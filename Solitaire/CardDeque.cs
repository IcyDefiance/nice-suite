﻿using System;
using System.Collections.Generic;
using OpenTK;
using nIceGame;
using Common.Scripts;

namespace nIceSuite
{
    class CardDeque : Interfaces.IDeque
    {
        public Vector3 Position { get; set; }
        protected LinkedList<Entity> cards;
        protected Vector3 layerOffset;
        protected Vector2 cardSize;

        public CardDeque(Vector3 position, Vector3 layerOffset, Vector2 cardSize)
        {
            cards = new LinkedList<Entity>();
            Position = position;
            this.layerOffset = layerOffset;
            this.cardSize = cardSize;
        }

        public void EnqueueFirst(Entity card)
        {
            if (card.Get<Transform>() != null)
                ScriptList.ReplaceSameType(card.Get<List<Script>>(), new SlideTo(card, Position, 0.05f));
            else
                card.Get<Transform>().Position = Position;

            LinkedListNode<Entity> repositionCard = cards.First;
            for (int i = 1; i <= cards.Count; i++)
            {
                ScriptList.ReplaceSameType(repositionCard.Value.Get<List<Script>>(), new SlideTo(repositionCard.Value, Position + Vector3.Multiply(layerOffset, i), 0.05f));
                repositionCard = repositionCard.Next;
            }

            cards.AddFirst(card);
        }

        public void EnqueueLast(Entity card)
        {
            if (card.Get<Transform>() != null)
                ScriptList.ReplaceSameType(card.Get<List<Script>>(), new SlideTo(card, Position + Vector3.Multiply(layerOffset, cards.Count), 0.05f));
            else
                card.Set(new Transform(Position + Vector3.Multiply(layerOffset, cards.Count), Quaternion.Identity, Vector3.One));

            cards.AddLast(card);
        }

        public Entity DequeueFirst()
        {
            Entity card = cards.First.Value;
            cards.RemoveFirst();

            LinkedListNode<Entity> repositionCard = cards.First;
            for (int i = 0; i < cards.Count; i++)
            {
                ScriptList.ReplaceSameType(repositionCard.Value.Get<List<Script>>(), new SlideTo(repositionCard.Value, Position + Vector3.Multiply(layerOffset, i), 0.05f));
                repositionCard = repositionCard.Next;
            }

            return card;
        }

        public Entity DequeueLast()
        {
            Entity card = cards.Last.Value;
            cards.RemoveLast();
            return card;
        }

        public int CountFaceUp()
        {
            int result = 0;

            foreach (Entity card in cards)
                if (card.Get<Components.CardInfo>().FaceUp)
                    result++;

            return result;
        }

        public Entity Last
        {
            get { return cards.Last == null ? null : cards.Last.Value; }
        }

        public Entity First
        {
            get { return cards.First.Value; }
        }

        public int Count
        {
            get { return cards.Count; }
        }

        public Rectangle BoundingBox
        {
            get { return new Rectangle((int)Position.X, (int)Position.Y, (int)(layerOffset.X * (cards.Count - 1) + cardSize.X), (int)(layerOffset.Y * (cards.Count - 1) + cardSize.Y)); }
        }
    }
}

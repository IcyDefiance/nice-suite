﻿using System;
using System.Collections.Generic;
using nIceGame;

namespace nIceSuite.Interfaces
{
    interface IDeque
    {
        void EnqueueFirst(Entity card);
        void EnqueueLast(Entity card);
        Entity DequeueFirst();
        Entity DequeueLast();
        Entity Last { get; }
        Entity First { get; }
        int Count { get; }
        Rectangle BoundingBox { get; }
    }
}

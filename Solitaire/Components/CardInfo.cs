﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace nIceSuite.Components
{
    enum CardColors { Red, Black }
    enum CardSuits { [Description("d")] Diamonds, [Description("c")] Clubs, [Description("h")] Hearts, [Description("s")] Spades }

    class CardInfo
    {
        public CardSuits CardSuit { get; set; }
        public int CardNumber { get; set; }
        public bool FaceUp { get; set; }

        public CardInfo(CardSuits suit, int number, bool faceUp = false)
        {
            CardSuit = suit;
            CardNumber = number;
            FaceUp = faceUp;
        }

        public CardColors CardColor
        {
            get
            {
                return CardSuit == CardSuits.Diamonds || CardSuit == CardSuits.Hearts ? CardColors.Red : CardColors.Black;
            }
        }

    }
}

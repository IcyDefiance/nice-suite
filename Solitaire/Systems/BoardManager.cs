﻿using System;
using System.Collections.Generic;
using nIceGame;
using OpenTK;
using OpenTK.Input;
using Common.Scripts;

namespace nIceSuite.Systems
{
    class BoardManager : ISystem
    {
        const int CARD_OFFSET = 20;
        const double DOUBLE_CLICK_THRESHOLD = 0.5;
        Entity[] allCards;
        CardDeque reserve;
        WasteStack waste;
        CardDeque[] foundation, tableau;
        DragDeque drag;
        Texture2D[,] cardTextures;
        Texture2D cardBackTex, placeholderTex;
        IAsyncResult loadResult;
        double lastClickTime;
        bool initialized, gameWon, disposed = false;

        public BoardManager()
        {
            initialized = false;       //load assets async
            AsyncLoadDelegate loadCaller = new AsyncLoadDelegate(Load);
            loadResult = loadCaller.BeginInvoke(null, null);

            Engine.GameWindow.Mouse.ButtonDown += Mouse_ButtonDown;
            Engine.GameWindow.Mouse.ButtonUp += Mouse_ButtonUp;
        }

        public void OnResize()
        {

        }

        public void OnUpdate(double delta)
        {
            if (!initialized)
            {
                if (loadResult.IsCompleted)
                {
                    Initialize();
                    Deal();
                    initialized = true;
                }
            }
            else if (!gameWon && foundation[0].Count == 13 && foundation[1].Count == 13 && foundation[2].Count == 13 && foundation[3].Count == 13)
            {
                bool wait = false;
                for (int i = 0; i < allCards.Length; i++)
                    if (ScriptList.HasScriptType(allCards[i].Get<List<Script>>(), typeof(SlideTo)))
                        wait = true;

                if (!wait)
                {
                    gameWon = true;
                    ScatterCards();
                }
            }

            if (lastClickTime > 0)
                lastClickTime -= delta;
        }

        public void OnRender(double delta)
        {

        }

        private delegate void AsyncLoadDelegate();
        private void Load()
        {
            cardBackTex = Content.LoadTexture2D("solitaire\\cardback", true, true);
            placeholderTex = Content.LoadTexture2D("solitaire\\placeholder", true, true);

            cardTextures = new Texture2D[4, 13];
            for (int i = 0; i < cardTextures.GetLength(0); i++)
                for (int j = 0; j < cardTextures.GetLength(1); j++)
                {
                    string fileString = ((Components.CardSuits)i).ToString() + (j % 13 + 1);
                    cardTextures[i, j] = Content.LoadTexture2D("solitaire\\" + fileString, true, true);
                }
        }

        public void NewGame()
        {
            if (initialized)
            {
                ResetBoard();
                Deal();
            }
        }

        private void Initialize()
        {
            cardBackTex.SendToGPU();
            placeholderTex.SendToGPU();
            for (int i = 0; i < cardTextures.GetLength(0); i++)
                for (int j = 0; j < cardTextures.GetLength(1); j++)
                    cardTextures[i, j].SendToGPU();

            allCards = new Entity[52];
            for (int i = 0; i < allCards.Length; i++)
            {
                allCards[i] = new Entity();
                allCards[i].Set(cardBackTex);
                allCards[i].Set(new List<Script>());
                allCards[i].Set(new Components.CardInfo((Components.CardSuits)(i / 13), i % 13 + 1));
                Engine.Entities.Add(allCards[i]);
            }

            ResetBoard();

            Entity[] placeHolders = new Entity[13];
            for (int i = 0; i < placeHolders.Length; i++)
            {
                placeHolders[i] = new Entity();
                placeHolders[i].Set(placeholderTex);
                placeHolders[i].Set(new Tint(1, 1, 1, 0.25f));
                Engine.Entities.Add(placeHolders[i]);
            }
            placeHolders[0].Set(new Transform(reserve.Position, Quaternion.Identity, Vector3.One));
            placeHolders[1].Set(new Transform(waste.Position, Quaternion.Identity, Vector3.One));
            for (int i = 0; i < 4; i++)
                placeHolders[i + 2].Set(new Transform(foundation[i].Position, Quaternion.Identity, Vector3.One));
            for (int i = 0; i < 7; i++)
                placeHolders[i + 6].Set(new Transform(tableau[i].Position, Quaternion.Identity, Vector3.One));
        }
        
        private void ResetBoard()
        {
            reserve = new CardDeque(new Vector3(50, 95, 10), new Vector3(0.25f, 0.25f, 1), new Vector2(cardBackTex.Width, cardBackTex.Height));
            waste = new WasteStack(new Vector3(175, 95, 10), new Vector3(CARD_OFFSET, 0, 1), new Vector2(cardBackTex.Width, cardBackTex.Height));
            foundation = new CardDeque[4];
            for (int i = 0; i < foundation.Length; i++)
                foundation[i] = new CardDeque(new Vector3(125 * i + 425, 95, 10), new Vector3(0, 0, 1), new Vector2(cardBackTex.Width, cardBackTex.Height));
            tableau = new CardDeque[7];
            for (int i = 0; i < tableau.Length; i++)
                tableau[i] = new CardDeque(new Vector3(125 * i + 50, 280, 10), new Vector3(0, CARD_OFFSET, 1), new Vector2(cardBackTex.Width, cardBackTex.Height));
        }

        private void Deal()
        {
            //shuffle into reserve
            List<Entity> tempDeck = new List<Entity>(allCards);
            Random random = new Random();
            for (int i = 0; i < allCards.Length; i++)
            {
                int nextIndex = random.Next(tempDeck.Count);
                reserve.EnqueueLast(tempDeck[nextIndex]);
                tempDeck.RemoveAt(nextIndex);
                if (reserve.Last.Get<Components.CardInfo>().FaceUp)
                    FlipCard(reserve.Last);
            }

            //deal to tableau
            for (int slot = 0; slot < tableau.Length; slot++)
            {
                for (int i = 0; i <= slot; i++)
                {
                    tableau[slot].EnqueueLast(reserve.DequeueLast());
                }

                FlipCard(tableau[slot].Last);
            }
        }

        private void ScatterCards()
        {
            Random random = new Random();
            for (int i = 0; i < 52; i++)
            {
                int rx = random.Next(-1000, 1001);
                int ry = random.Next(-1000, 1001);
                if (rx > 0)
                    rx += Engine.GameWindow.Width;
                else
                    rx -= cardBackTex.Width;
                if (ry > 0)
                    ry += Engine.GameWindow.Height;
                else
                    ry -= cardBackTex.Height;

                ScriptList.ReplaceSameType(allCards[i].Get<List<Script>>(), new SlideTo(allCards[i], new Vector3(rx, ry, 10), 0.1f));
            }
        }

        private void FlipCard(Entity card)
        {
            if (card.Get<Components.CardInfo>().FaceUp)
            {
                card.Set(cardBackTex);
                card.Get<Components.CardInfo>().FaceUp = false;
            }
            else
            {
                card.Set(cardTextures[(int)card.Get<Components.CardInfo>().CardSuit, (int)card.Get<Components.CardInfo>().CardNumber - 1]);
                card.Get<Components.CardInfo>().FaceUp = true;
            }
        }

        private void Mouse_ButtonDown(object sender, OpenTK.Input.MouseButtonEventArgs e)
        {
            if (e.Button != MouseButton.Left)
                return;

            bool isDoubleClick = lastClickTime > 0;
            if (isDoubleClick)
                lastClickTime = -1;
            else
                lastClickTime = DOUBLE_CLICK_THRESHOLD;
            
            if (reserve.BoundingBox.Contains(e.X, e.Y))
            {
                if (reserve.Count > 0)
                {
                    for (int i = 0; i < 3 && reserve.Count > 0; i++)
                    {
                        FlipCard(reserve.Last);
                        waste.EnqueueLast(reserve.DequeueLast());
                    }
                }
                else
                {
                    while (waste.Count > 0)
                    {
                        reserve.EnqueueLast(waste.DequeueLast());
                        FlipCard(reserve.Last);
                    }
                }
            }
            else if (waste.BoundingBox.Contains(e.X, e.Y))
            {
                if (waste.Last != null)
                {
                    if (isDoubleClick)
                    {
                        bool found = false;
                        for (int i = 0; i < foundation.Length && !found; i++)
                        {
                            Components.CardInfo cardInfo = waste.Last.Get<Components.CardInfo>();
                            Components.CardInfo foundationTopInfo = foundation[i].Last == null ? null : foundation[i].Last.Get<Components.CardInfo>();
                            if ((foundationTopInfo == null &&
                                    cardInfo.CardNumber == 1) ||
                                (foundationTopInfo != null && 
                                    foundationTopInfo.CardSuit == cardInfo.CardSuit &&
                                    foundationTopInfo.CardNumber == cardInfo.CardNumber - 1))
                            {
                                found = true;
                                foundation[i].EnqueueLast(waste.DequeueLast());
                            }
                        }
                    }
                    else
                    {
                        drag = new DragDeque(waste.Last.Get<Transform>().Position, new Vector3(0, CARD_OFFSET, 1), new Vector2(cardBackTex.Width, cardBackTex.Height), waste);
                        drag.EnqueueFirst(waste.DequeueLast());
                    }
                }
            }
            else
            {
                bool found = false;
                for (int i = 0; i < tableau.Length && !found; i++)
                {
                    if (tableau[i].BoundingBox.Contains(e.X, e.Y))
                    {
                        found = true;

                        int cardsSelected = tableau[i].Count - ((int)(e.Y - tableau[i].Position.Y) / CARD_OFFSET);
                        if (cardsSelected < 1)
                            cardsSelected = 1;

                        if (isDoubleClick && cardsSelected == 1)
                        {
                            bool foundDest = false;
                            if (tableau[i].Last != null)
                            {
                                for (int j = 0; j < foundation.Length && !foundDest; j++)
                                {
                                    Components.CardInfo cardInfo = tableau[i].Last.Get<Components.CardInfo>();
                                    Components.CardInfo foundationTopInfo = foundation[j].Last == null ? null : foundation[j].Last.Get<Components.CardInfo>();
                                    if ((foundationTopInfo == null &&
                                            cardInfo.CardNumber == 1) ||
                                        (foundationTopInfo != null &&
                                            foundationTopInfo.CardSuit == cardInfo.CardSuit &&
                                            foundationTopInfo.CardNumber == cardInfo.CardNumber - 1))
                                    {
                                        foundDest = true;
                                        foundation[j].EnqueueLast(tableau[i].DequeueLast());
                                        if (tableau[i].Last != null && tableau[i].Last.Get<Components.CardInfo>().FaceUp == false)
                                            FlipCard(tableau[i].Last);
                                    }
                                }
                            }
                        }
                        else if (cardsSelected <= tableau[i].CountFaceUp())
                        {
                            Stack<Entity> tempStack = new Stack<Entity>(cardsSelected);
                            for (int j = 0; j < cardsSelected; j++)
                                tempStack.Push(tableau[i].DequeueLast());

                            drag = new DragDeque(tempStack.Peek().Get<Transform>().Position, new Vector3(0, CARD_OFFSET, 1), new Vector2(cardBackTex.Width, cardBackTex.Height), tableau[i]);
                            drag.EnqueueFirst(tempStack.Pop());
                            while (tempStack.Count > 0)
                            {
                                drag.EnqueueLast(tempStack.Pop());
                            }
                        }
                    }
                }

                for (int i = 0; i < foundation.Length && !found; i++)
                {
                    if (foundation[i].BoundingBox.Contains(e.X, e.Y) && foundation[i].Count > 0)
                    {
                        drag = new DragDeque(foundation[i].Last.Get<Transform>().Position, new Vector3(0, CARD_OFFSET, 1), new Vector2(cardBackTex.Width, cardBackTex.Height), foundation[i]);
                        drag.EnqueueFirst(foundation[i].DequeueLast());
                    }
                }
            }
        }

        private void Mouse_ButtonUp(object sender, OpenTK.Input.MouseButtonEventArgs e)
        {
            if (e.Button != MouseButton.Left)
                return;

            if (drag == null)
                return;

            Components.CardInfo dragFirstInfo = drag.First.Get<Components.CardInfo>();

            for (int i = 0; i < tableau.Length && drag.Count > 0; i++)
            {
                Components.CardInfo tableauLastInfo = tableau[i].Last == null ? null : tableau[i].Last.Get<Components.CardInfo>();
                if (tableau[i].BoundingBox.Contains(e.X, e.Y) &&
                    (tableauLastInfo == null ||
                    (dragFirstInfo.CardColor != tableauLastInfo.CardColor &&
                    dragFirstInfo.CardNumber == tableauLastInfo.CardNumber - 1)))
                {
                    while (drag.Count > 0)
                    {
                        tableau[i].EnqueueLast(drag.DequeueFirst());
                    }
                }
            }

            for (int i = 0; i < foundation.Length && drag.Count > 0; i++)
            {
                Components.CardInfo foundationLastInfo = foundation[i].Last == null ? null : foundation[i].Last.Get<Components.CardInfo>();
                if (foundation[i].BoundingBox.Contains(e.X, e.Y) &&
                    drag.Count == 1 &&
                    ((foundationLastInfo == null &&
                        dragFirstInfo.CardNumber == 1) ||
                    (foundationLastInfo != null &&
                        (dragFirstInfo.CardSuit == foundationLastInfo.CardSuit &&
                        dragFirstInfo.CardNumber == foundationLastInfo.CardNumber + 1))))
                {
                    foundation[i].EnqueueLast(drag.DequeueFirst());
                }
            }

            if (drag.Count == 0 && drag.Source.Count > 0 && drag.Source.Last.Get<Components.CardInfo>().FaceUp == false)
                FlipCard(drag.Source.Last);
            else
                while (drag.Count > 0)
                    drag.Source.EnqueueLast(drag.DequeueFirst());

            drag.Dispose();
            drag = null;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                Engine.GameWindow.Mouse.ButtonDown -= Mouse_ButtonDown;
                Engine.GameWindow.Mouse.ButtonUp -= Mouse_ButtonUp;
                if (drag != null)
                    drag.Dispose();

                cardBackTex.Dispose();
                placeholderTex.Dispose();
                for (int i = 0; i < cardTextures.GetLength(0); i++)
                    for (int j = 0; j < cardTextures.GetLength(1); j++)
                        cardTextures[i, j].Dispose();
            }

            disposed = true;
        }
    }
}

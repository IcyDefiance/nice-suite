﻿using System;
using System.Collections.Generic;
using OpenTK;
using nIceGame;
using Common.Scripts;

namespace nIceSuite
{
    class DragDeque : CardDeque, IDisposable
    {
        public Interfaces.IDeque Source { get; private set; }
        private bool disposed = false;

        public DragDeque(Vector3 position, Vector3 layerOffset, Vector2 cardSize, Interfaces.IDeque source)
            : base(position + new Vector3(0, 0, 100), layerOffset, cardSize)
        {
            Source = source;
            Engine.GameWindow.Mouse.Move += Mouse_Move;
        }

        public new void EnqueueFirst(Entity card)
        {
            ScriptList.RemoveScriptType((List<Script>)card.Get<List<Script>>(), typeof(SlideTo));
            card.Get<Transform>().Position = Position;

            LinkedListNode<Entity> repositionCard = cards.First;
            for (int i = 1; i <= cards.Count; i++)
            {
                repositionCard.Value.Get<Transform>().Position = Position + Vector3.Multiply(layerOffset, i);
                repositionCard = repositionCard.Next;
            }

            Vector3 newPos = card.Get<Transform>().Position;
            newPos.Z = Position.Z;
            card.Get<Transform>().Position = newPos;
            cards.AddFirst(card);
        }

        public new void EnqueueLast(Entity card)
        {
            ScriptList.RemoveScriptType(card.Get<List<Script>>(), typeof(SlideTo));
            card.Get<Transform>().Position = Position + Vector3.Multiply(layerOffset, cards.Count);

            cards.AddLast(card);
        }

        void Mouse_Move(object sender, OpenTK.Input.MouseMoveEventArgs e)
        {
            foreach (Entity card in cards)
            {
                Transform transform = card.Get<Transform>();
                transform.Position += new Vector3(e.XDelta, e.YDelta, 0);
            }
        }

        public void Dispose()
        {
            if (!disposed)
            {
                Engine.GameWindow.Mouse.Move -= Mouse_Move;
                disposed = true;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using OpenTK;
using nIceGame;
using Common.Scripts;

namespace nIceSuite
{
    class WasteStack : Interfaces.IDeque
    {
        private CardDeque stackTop, stackBottom;
        private Vector3 layerOffset;
        private Vector2 cardSize;

        public WasteStack(Vector3 position, Vector3 layerOffset, Vector2 cardSize)
        {
            stackTop = new CardDeque(new Vector3(position.X, position.Y, position.Z + 50) + layerOffset, layerOffset, cardSize);
            stackBottom = new CardDeque(position, Vector3.UnitZ, cardSize);
            this.layerOffset = layerOffset;
            this.cardSize = cardSize;
        }

        public void EnqueueLast(Entity card)
        {
            stackTop.EnqueueLast(card);
            if (stackTop.Count > 2)
                stackBottom.EnqueueLast(stackTop.DequeueFirst());
        }

        public Entity DequeueLast()
        {
            if (stackTop.Count > 0)
                return stackTop.DequeueLast();
            else
                return stackBottom.DequeueLast();
        }

        public int Count
        {
            get { return stackTop.Count + stackBottom.Count; }
        }

        public Rectangle BoundingBox
        {
            get { return new Rectangle((int)((stackTop.Count - 1) * layerOffset.X + stackTop.Position.X), (int)((stackTop.Count - 1) * layerOffset.Y + stackTop.Position.Y), (int)cardSize.X, (int)cardSize.Y); }
        }

        public void EnqueueFirst(Entity card)
        {
            throw new NotImplementedException();
        }

        public Entity DequeueFirst()
        {
            throw new NotImplementedException();
        }

        public Vector3 Position
        {
            get { return stackBottom.Position; }
        }

        public Entity Last
        {
            get { return stackTop.Count > 0 ? stackTop.Last : stackBottom.Last; }
        }

        public Entity First
        {
            get { return stackBottom.First; }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Timers;
using nIceGame;
using Common.Scripts;
using OpenTK;
using OpenTK.Input;
using OpenTK.Graphics;

namespace nIceSuite.Systems
{
    class MenuSystem : ISystem
    {
        public Scene SwitchingSceneTo { get; set; }

        private List<Entity> menuItems;
        private List<string> menuItemNames;
        private Entity background, logo, menuBackground, highlight;
        private int itemSelected = 0;

        public MenuSystem(Entity background, Entity logo)
        {
            this.background = background;
            this.logo = logo;

            Engine.GameWindow.Keyboard.KeyDown += OnKeyDown;
            Engine.GameWindow.Mouse.Move += Mouse_Move;
            Engine.GameWindow.Mouse.ButtonDown += Mouse_ButtonDown;

            //menu
            menuBackground = new Entity();
            menuBackground.Set(Texture2D.WhitePixel);
            menuBackground.Set(new Transform(new Vector3(150, 175, 0), Quaternion.Identity, new Vector3(400, Engine.GameWindow.Height - 175, 1)));
            menuBackground.Set(new Tint(0, 0, 0, 0));
            menuBackground.Set(new List<Script>());
            Engine.Entities.Add(menuBackground);

            highlight = new Entity();
            highlight.Set(Texture2D.WhitePixel);
            highlight.Set(new Transform(new Vector3(150, 195, 0), Quaternion.Identity, new Vector3(10, 50, 1)));
            highlight.Set(new Tint(0, 0.75f, 1, 0));
            highlight.Set(new List<Script>());
            Engine.Entities.Add(highlight);

            //menu items
            menuItems = new List<Entity>();
            menuItemNames = new List<string>();

            Entity solitaire = new Entity();
            solitaire.Set(Content.LoadTexture2D("solitaire", true));
            solitaire.Set(new Transform(new Vector3(175, 200, 0), Quaternion.Identity, Vector3.One));
            solitaire.Set(new Tint(1, 1, 1, 0));
            solitaire.Set(new List<Script>());
            Engine.Entities.Add(solitaire);
            menuItems.Add(solitaire);
            menuItemNames.Add("Solitaire");

            Entity minesweeper = new Entity();
            minesweeper.Set(Content.LoadTexture2D("minesweeper", true));
            minesweeper.Set(new Transform(new Vector3(175, 250, 0), Quaternion.Identity, Vector3.One));
            minesweeper.Set(new Tint(1, 1, 1, 0));
            minesweeper.Set(new List<Script>());
            Engine.Entities.Add(minesweeper);
            menuItems.Add(minesweeper);
            menuItemNames.Add("Minesweeper");

            Entity moreSoon = new Entity();
            moreSoon.Set(Content.LoadTexture2D("moreSoon", true));
            moreSoon.Set(new Transform(new Vector3(175, 300, 0), Quaternion.Identity, Vector3.One));
            moreSoon.Set(new Tint(0.5f, 0.5f, 0.5f, 0));
            moreSoon.Set(new List<Script>());
            Engine.Entities.Add(moreSoon);
            menuItems.Add(moreSoon);
            menuItemNames.Add("More coming soonTM");

            //timers
            Timer fadeInDelay = new Timer(1000);
            fadeInDelay.Elapsed += OnFadeInDelay;
            fadeInDelay.Start();
        }

        public void OnRender(double delta)
        {
        }

        public void OnResize()
        {
        }

        public void OnUpdate(double delta)
        {
        }

        private void OnKeyDown(object sender, KeyboardKeyEventArgs e)
        {
            if (SwitchingSceneTo != null)
                return;

            if ((e.Key == Key.S || e.Key == Key.Down) && itemSelected < menuItems.Count - 1)
            {
                itemSelected++;
                MoveHighlight();
            }
            else if ((e.Key == Key.W || e.Key == Key.Up) && itemSelected > 0)
            {
                itemSelected--;
                MoveHighlight();
            }
            else if (e.Key == Key.Enter || e.Key == Key.KeypadEnter || e.Key == Key.Space)
            {
                EndScene(menuItemNames[itemSelected]);
            }
        }

        void Mouse_ButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.Button != MouseButton.Left)
                return;

            Vector3 itemTransform = (menuItems[itemSelected].Get<Transform>()).Position + new Vector3(-25, -5, 0);
            Rectangle itemRectangle = new Rectangle((int)itemTransform.X, (int)itemTransform.Y, 400, 50);
            if (itemRectangle.Contains(e.X, e.Y))
            {
                EndScene(menuItemNames[itemSelected]);
            }
        }

        void Mouse_Move(object sender, MouseMoveEventArgs e)
        {
            bool movedHighlight = false;
            for (int i = 0; i < menuItems.Count && !movedHighlight; i++)
            {
                Vector3 itemTransform = (menuItems[i].Get<Transform>()).Position + new Vector3(-25, -5, 0);
                Rectangle itemRectangle = new Rectangle((int)itemTransform.X, (int)itemTransform.Y, 400, 50);
                if (itemRectangle.Contains(e.X, e.Y))
                {
                    itemSelected = i;
                    MoveHighlight();
                    movedHighlight = true;
                }
            }
        }

        private void MoveHighlight()
        {
            Vector3 highlightPos = (highlight.Get<Transform>()).Position;
            Vector3 itemPos = (menuItems[itemSelected].Get<Transform>()).Position;

            SlideTo newSlideTo = new Common.Scripts.SlideTo(highlight, new Vector3(highlightPos.X, itemPos.Y - 5, highlightPos.Z), 0.1f);
            ScriptList.ReplaceSameType(highlight.Get<List<Script>>(), newSlideTo);
        }

        private void EndScene(string switchTo)
        {
            switch (switchTo)
            {
                case "Solitaire":
                    SwitchingSceneTo = new SceneSolitaire(background, logo);
                    break;
                case "Minesweeper":
                    SwitchingSceneTo = new MinesweeperScene(background, logo);
                    break;
                default:
                    return;
            }

            for (int i = 0; i < menuItems.Count; i++)
                ScriptList.ReplaceSameType(menuItems[i].Get<List<Script>>(), new FadeTo(menuItems[i], 0, 4.8f));

            ScriptList.ReplaceSameType(menuBackground.Get<List<Script>>(), new FadeTo(menuBackground, 0, 2.4f));
            ScriptList.ReplaceSameType(highlight.Get<List<Script>>(), new FadeTo(highlight, 0, 2.4f));

            Engine.GameWindow.Keyboard.KeyDown -= OnKeyDown;
            Engine.GameWindow.Mouse.Move -= Mouse_Move;
            Engine.GameWindow.Mouse.ButtonDown -= Mouse_ButtonDown;
        }

        private void OnFadeInDelay(object sender, ElapsedEventArgs e)
        {
            ((Timer)sender).Stop();
            ((Timer)sender).Elapsed -= OnFadeInDelay;
            FadeIn();
        }

        public void OnRegainFocus()
        {
            FadeIn();

            Engine.GameWindow.Keyboard.KeyDown += OnKeyDown;
            Engine.GameWindow.Mouse.Move += Mouse_Move;
            Engine.GameWindow.Mouse.ButtonDown += Mouse_ButtonDown;
        }

        private void FadeIn()
        {
            for (int i = 0; i < menuItems.Count; i++)
                ScriptList.ReplaceSameType(menuItems[i].Get<List<Script>>(), new FadeTo(menuItems[i], 1, 4.8f));

            ScriptList.ReplaceSameType(menuBackground.Get<List<Script>>(), new FadeTo(menuBackground, 0.5f, 2.4f));
            ScriptList.ReplaceSameType(highlight.Get<List<Script>>(), new FadeTo(highlight, 0.5f, 2.4f));
        }

        public void Dispose()
        {
        }
    }
}

﻿using System;
using System.Threading;
using System.Timers;
using System.Collections.Generic;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;
using nIceGame;
using Common.Scripts;

namespace nIceSuite
{
    class SceneMenu : Scene
    {
        System.Timers.Timer sceneEndTimer;
        Systems.MenuSystem menuSystem;
        Entity logo, background;
        bool sceneEnding = false, sceneEnded = false;

        public override void OnLoad()
        {
            base.OnLoad();

            //settings
            GL.Enable(EnableCap.CullFace);
            GL.Enable(EnableCap.DepthTest);
            Engine.GameWindow.VSync = VSyncMode.On;
            Engine.GameWindow.WindowBorder = WindowBorder.Fixed;

            //sprites
            background = new Entity();
            background.Name = "background";
            Texture2D backgroundTexture = Content.LoadTexture2D("background", true);
            background.Set(backgroundTexture);
            float scale = Math.Max((float)Engine.GameWindow.Height / backgroundTexture.Height, (float)Engine.GameWindow.Width / backgroundTexture.Width);
            background.Set(new Transform(Vector3.Zero, Quaternion.Identity, new Vector3(scale, scale, 1)));
            Engine.Entities.Add(background);

            logo = new Entity();
            logo.Name = "logo";
            logo.Set(Content.LoadTexture2D("cubeLogo", true));
            logo.Set(new Transform(new Vector3(50, 720, 0), Quaternion.Identity, Vector3.One));
            List<Script> logoScripts = new List<Script>();
            logoScripts.Add(new Common.Scripts.SlideTo(logo, new Vector3(50, 50, 0), 0.05f));
            logo.Set(logoScripts);
            Engine.Entities.Add(logo);

            //systems
            Engine.Systems.Add(new MeshRenderer());
            Engine.Systems.Add(new SpriteRenderer());
            Engine.Systems.Add(new ScriptRunner());
            menuSystem = new Systems.MenuSystem(background, logo);
            Engine.Systems.Add(menuSystem);
        }

        public override void UpdateFrame(object sender, OpenTK.FrameEventArgs e)
        {
            base.UpdateFrame(sender, e);

            if (Engine.GameWindow.Keyboard[Key.Escape])
                Engine.GameWindow.Exit();

            if (menuSystem.SwitchingSceneTo != null)
            {
                if (!sceneEnding)
                {
                    ScriptList.ReplaceSameType(logo.Get<List<Script>>(), new SlideTo(logo, new Vector3(10, 10, 0), 0.1f));
                    ScriptList.ReplaceSameType(logo.Get<List<Script>>(), new ScaleTo(logo, new Vector3(0.5f), new Vector3(0.03f)));
                    sceneEndTimer = new System.Timers.Timer(500);
                    sceneEndTimer.Elapsed += sceneEndTimer_Elapsed;
                    sceneEndTimer.Start();
                    sceneEnding = true;
                }
                else if (sceneEnded)
                {
                    Engine.PushScene(menuSystem.SwitchingSceneTo);
                    menuSystem.SwitchingSceneTo = null;
                    sceneEnding = false;
                    sceneEnded = false;
                }
            }
        }

        public override void RenderFrame(object sender, OpenTK.FrameEventArgs e)
        {
            base.RenderFrame(sender, e);

            Engine.GameWindow.SwapBuffers();
        }

        public override void OnRegainFocus()
        {
            base.OnRegainFocus();

            Engine.GameWindow.ClientSize = new System.Drawing.Size(1440, 768);
            float scale = Math.Max((float)Engine.GameWindow.Height / background.Get<Texture2D>().Height, (float)Engine.GameWindow.Width / background.Get<Texture2D>().Width);
            background.Get<Transform>().Position = Vector3.Zero;
            background.Get<Transform>().Scale = new Vector3(scale, scale, 1);
            ScriptList.ReplaceSameType(logo.Get<List<Script>>(), new SlideTo(logo, new Vector3(50, 50, 0), 0.1f));
            ScriptList.ReplaceSameType(logo.Get<List<Script>>(), new ScaleTo(logo, Vector3.One, new Vector3(0.03f)));
            menuSystem.OnRegainFocus();
        }

        void sceneEndTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            sceneEnded = true;
        }
    }
}

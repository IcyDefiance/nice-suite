﻿using System;
using System.Collections.Generic;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Audio;
using OpenTK.Audio.OpenAL;
using OpenTK.Input;
using nIceGame;

namespace nIceSuite
{
    class nIceSuite : GameWindow
    {
        public nIceSuite()
            : base(1440, 768, GraphicsMode.Default, "nIce Suite")
        {
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
        }

        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            base.OnUpdateFrame(e);
        }

        protected override void OnRenderFrame(FrameEventArgs e)
        {
            base.OnRenderFrame(e);
        }

        [STAThread]
        static void Main()
        {
            using (nIceSuite game = new nIceSuite())
            {
                Engine.Initialize(game);
                Engine.PushScene(new SceneMenu());
                game.Run(60, 60);
                Engine.PopScene();
            }
        }
    }
}